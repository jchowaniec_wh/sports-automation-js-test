const { When, Then } = require('cucumber');

const sportsMenuComponent = require('../../components/sportsMenu.component');
const productMenuComponent = require('../../components/productMenu.component');

const sportsMenu = new sportsMenuComponent();
const productMenu = new productMenuComponent();

When(/^sports menu is displayed$/, function () {
    expect(sportsMenu.isDisplayed()).toBeTruthy();
});

When(/^the product menu is displayed$/, function () {
    expect(productMenu.isDisplayed()).toBeTruthy();
});
