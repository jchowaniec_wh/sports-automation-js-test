const { When, Then } = require('cucumber');

const homePage = require('../../pages/home.page.js');
const home = new homePage();

When(/^user opens Homepage page$/, function () {
    home.open();
});

Then(/^homepage is displayed$/, function () {
    expect(home.isDisplayed()).toBeTruthy();
});
