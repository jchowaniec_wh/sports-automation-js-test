const { When, Then } = require('cucumber');

const betslipComponent = require('../../components/betslip.component');
const betslip = new betslipComponent();

Then(/^betslip is displayed$/, function () {
    expect(betslip.isDisplayed()).toBeTruthy();
});
