class homePage  {

 get homeSection() {return $('#main-container');}

  open() {
    browser.url('https://sports.williamhill.com/betting/en-gb');
  }

  isDisplayed() {
    return this.homeSection.isDisplayed();
  }

}

module.exports = homePage;
