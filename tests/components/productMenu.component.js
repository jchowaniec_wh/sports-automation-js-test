class productMenuComponent {

    get navigationMenu() {return $('[data-test-id="pre-integrated-header"]');}

    isDisplayed() {
        return this.navigationMenu.isDisplayed();
    }
}

module.exports = productMenuComponent;
