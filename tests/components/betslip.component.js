class betslipComponent {

    get betslipSection() {return $('.bs-betslip');}

    isDisplayed() {
        return this.betslipSection.isExisting();
    }
}

module.exports = betslipComponent;
