class sportsMenuComponent {

    get desktopSidebar() {return $('#desktop-sidebar');}
    get desktopSidebarAllSports() {return $('#desktop-sidebar-az');}
    get desktopSidebarHead() {return $('#desktop-sidebar-head');}
    get desktopSidebarExtras() {return $('#desktop-sidebar-extras');}

    isDisplayed() {
        return this.desktopSidebar.isDisplayed() &&
            this.desktopSidebarAllSports.isDisplayed() &&
            this.desktopSidebarHead.isDisplayed() &&
            this.desktopSidebarExtras.isDisplayed();
    }

}

module.exports = sportsMenuComponent;
